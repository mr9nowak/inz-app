import { Injectable } from '@angular/core'
import { auth } from 'firebase/app'
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore } from '@angular/fire/firestore'
import { Router } from '@angular/router'
import { NzNotificationService } from 'ng-zorro-antd'
import {ApiService} from './api.service'

interface User {
  uid: string
  email: string
  displayName: string
  photoURL: string
  emailVerified: boolean
  role: string
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userData: any
  token: any

  constructor(
    private apiService: ApiService,
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    private notification: NzNotificationService,
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user
        localStorage.setItem('user', JSON.stringify(this.userData))
      } else {
        localStorage.setItem('user', null)
      }
    })
  }

  async SignIn(email: string, password: string) {
    await this.apiService.login(email, password).subscribe(
      (data) => {
        if (data) {
          // @ts-ignore
          this.token = data.token
          localStorage.setItem('token', this.token)

          this.notification.success(
            'Zalogowany',
            'Pomyślnie zalogowałeś się',
          )
          this.router.navigate(['admin/tickets'])
        }
      },
      (error) => {
        error = error.error
        this.notification.error(error.code, error.message)
        localStorage.setItem('token', null)
      }
    )
  }

  get isLoggedIn(): boolean {
    const token = localStorage.getItem('token')
    return token !== null
  }

  async SignOut() {
    await this.afAuth.auth.signOut()
    localStorage.removeItem('token')
    this.router.navigate(['admin/login'])
  }
}
