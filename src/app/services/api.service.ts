import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

const API_URL = 'http://api.pomoc.jakubnowak.pl'

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { }

  public closeTicket(id) {
    return this.httpClient.post(API_URL + '/api/v1/tickets/' + id + '/close', {}, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public createTicket(ticket) {
    return this.httpClient.post(API_URL + '/api/v1/tickets', ticket)
  }

  public createWorker(worker) {
    return this.httpClient.post(API_URL + '/api/v1/workers', worker, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public editWorker(worker) {
    return this.httpClient.patch(API_URL + '/api/v1/workers/' + worker.uuid, worker, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public setEstimateTicket(ticket) {
    return this.httpClient.post(API_URL + '/api/v1/tickets/' + ticket.uuid + '/estimate', {estimate: ticket.estimate_time}, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public assignWorkerTicket(id, worker) {
    return this.httpClient.post(API_URL + '/api/v1/tickets/' + id + '/assign-worker', {worker: worker}, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public setSpendTicket(ticket) {
    return this.httpClient.post(API_URL + '/api/v1/tickets/' + ticket.uuid + '/spend', {spend: ticket.spend_time}, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public getTickets() {
    return this.httpClient.get(API_URL + '/api/v1/tickets', {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public getTicket(id) {
    return this.httpClient.get(API_URL + '/api/v1/tickets/' + id, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public getWorker(id) {
    return this.httpClient.get(API_URL + '/api/v1/workers/' + id, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public getWorkers() {
    return this.httpClient.get(API_URL + '/api/v1/workers', {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public getMe() {
    return this.httpClient.get(API_URL + '/api/v1/me', {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
  }

  public login(username, password) {
    return this.httpClient.post(API_URL + '/auth/login', {
      _username: username,
      _password: password,
    })
  }
}
