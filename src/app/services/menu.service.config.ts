export const getMenuData: any[] = [
  {
    title: 'Zgłoszenia',
    key: 'tickets',
    icon: 'fe fe-list',
    url: '/admin/tickets',
  },
  {
    title: 'Pracownicy',
    key: 'workers',
    icon: 'fe fe-users',
    url: '/admin/workers',
  },
]
