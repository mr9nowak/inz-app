import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthService } from 'src/app/services/auth.service'
import { AuthGuard } from 'src/app/components/layout/Guard/auth.guard'
import { LayoutsModule } from 'src/app/layouts/layouts.module'

// Apps
import { AppsMessagingComponent } from 'src/app/pages/apps/messaging/messaging.component'
import { AppsCalendarComponent } from 'src/app/pages/apps/calendar/calendar.component'
import { AppsProfileComponent } from 'src/app/pages/apps/profile/profile.component'
import { AppsGalleryComponent } from 'src/app/pages/apps/gallery/gallery.component'
import { AppsMailComponent } from 'src/app/pages/apps/mail/mail.component'
import { WorkerComponent } from './worker/worker.component'
import { TicketsComponent } from './tickets/tickets.component'
import { WorkerCreateComponent } from './worker-create/worker-create.component'
import { WorkerEditComponent } from './worker-edit/worker-edit.component'
import { TicketShowComponent } from './ticket-show/ticket-show.component'

const routes: Routes = [
  {
    path: 'tickets',
    component: TicketsComponent,
    data: { title: 'Zgłoszenia' },
    canActivate: [AuthGuard],
  },
  {
    path: 'tickets/:id',
    component: TicketShowComponent,
    data: { title: 'Zgłoszenia' },
    canActivate: [AuthGuard],
  },
  {
    path: 'workers',
    component: WorkerComponent,
    data: { title: 'Pracownicy' },
    canActivate: [AuthGuard],
  },
  {
    path: 'workers/create',
    component: WorkerCreateComponent,
    data: { title: 'Pracownicy' },
    canActivate: [AuthGuard],
  },
  {
    path: 'workers/edit/:id',
    component: WorkerEditComponent,
    data: { title: 'Pracownicy' },
    canActivate: [AuthGuard],
  },
]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  providers: [AuthService],
  exports: [RouterModule],
})
export class AppsRouterModule {}
