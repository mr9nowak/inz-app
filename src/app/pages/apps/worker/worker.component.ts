import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service'

@Component({
  selector: 'app-workers',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.scss']
})
export class WorkerComponent implements OnInit {
  workers: Object = []

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getWorkers().subscribe((data) => {
      this.workers = data
    })
  }
}
