import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NzNotificationService } from 'ng-zorro-antd'
import { ActivatedRoute, Router } from '@angular/router'
import { Worker } from '../../../models/worker'

@Component({
  selector: 'app-worker-edit',
  templateUrl: './worker-edit.component.html',
  styleUrls: ['./worker-edit.component.scss']
})
export class WorkerEditComponent implements OnInit {
  editForm: FormGroup
  worker: any

  constructor(
    private api: ApiService,
    private fb: FormBuilder,
    public router: Router,
    private notification: NzNotificationService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.api.getWorker(params['id']).subscribe(
        (worker: Worker) => {
          this.worker = worker

          this.editForm.setValue({
            uuid: worker.uuid,
            first_name: worker.first_name,
            last_name: worker.last_name
          })
        },
        error => {
          this.router.navigateByUrl('/not-found', {replaceUrl: true});
        }
      )
    })
  }

  ngOnInit() {
    this.editForm = this.fb.group({
      uuid: [null],
      first_name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
    })
  }

  submitForm(): void {
    if (this.editForm.valid) {
      this.api.editWorker(this.editForm.value).subscribe(
        data => {
          this.notification.success(
            'Zapisano',
            ''
          )
          this.router.navigate(['admin/workers'])
        },
        error => {
          this.notification.error(error.code, error.message)
        }
        )
    }

    for (const i in this.editForm.controls) {
      this.editForm.controls[i].markAsDirty();
      this.editForm.controls[i].updateValueAndValidity();
    }
  }

  getErrorMessage(field: string) {
    switch (field) {
      case 'first_name': return this.editForm.controls.first_name.hasError('required') ? 'Pole jest wymagane' : '';
      case 'last_name': return this.editForm.controls.last_name.hasError('required') ? 'Pole jest wymagane' : '';
    }
  }
}
