import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service'
import {Ticket} from '../../../models/Ticket'
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {
  tickets: Ticket[] = []

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getTickets().subscribe((data: Ticket[]) => {
      this.tickets = data.map((item) => (new Ticket()).deserialize(item))
    })
  }
}
