import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service'
import { ActivatedRoute, Router } from '@angular/router'
import { Ticket } from '../../../models/Ticket'
import { Worker } from '../../../models/worker'

@Component({
  selector: 'app-ticket-show',
  templateUrl: './ticket-show.component.html',
  styleUrls: ['./ticket-show.component.scss']
})
export class TicketShowComponent implements OnInit {
  ticket: Ticket
  workers: Array<Worker>
  worker: string|null

  constructor(
    private api: ApiService,
    public router: Router,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.api.getTicket(params['id']).subscribe(
        (response) => {
          this.ticket = (new Ticket()).deserialize(response)

          this.worker = this.ticket.worker !== null ? this.ticket.worker.uuid : null

          this.api.getWorkers().subscribe((data: Worker[]) => {
            this.workers = data.map((item) => new Worker().deserialize(item))
          })
        },
        error => {
          this.router.navigateByUrl('/not-found', {replaceUrl: true});
        }
      )
    })
  }

  closeTicket(event) {
    event.preventDefault()

    this.api.closeTicket(this.ticket.uuid).subscribe(
      (response) => {
        this.ticket = (new Ticket()).deserialize(response)
      },
      error => {
      }
    )
  }

  changeEstimateTime(event) {
    this.ticket.estimate_time = event

    if (this.ticket.estimate_time > 0) {
      this.api.setEstimateTicket(this.ticket).subscribe(
        (response) => {
          this.ticket = (new Ticket()).deserialize(response)
        },
        error => {
        }
      )
    }
  }
  changeSpendTime(event) {
    this.ticket.spend_time = event

    if (this.ticket.spend_time > 0) {
      this.api.setSpendTicket(this.ticket).subscribe(
        (response) => {
          this.ticket = (new Ticket()).deserialize(response)
        },
        error => {
        }
      )
    }
  }

  assignWorker(event) {
    this.api.assignWorkerTicket(this.ticket.uuid, event).subscribe(
      (response) => {
        this.ticket = (new Ticket()).deserialize(response)
      },
      error => {
      }
    )
  }

  ngOnInit() {
  }
}
