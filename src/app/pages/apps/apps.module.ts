import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { AppsRouterModule } from './apps-routing.module'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar'
import { WidgetsComponentsModule } from 'src/app/components/widgets/widgets-components.module'

// Apps
import { AppsMessagingComponent } from 'src/app/pages/apps/messaging/messaging.component'
import { AppsCalendarComponent } from 'src/app/pages/apps/calendar/calendar.component'
import { AppsProfileComponent } from 'src/app/pages/apps/profile/profile.component'
import { AppsGalleryComponent } from 'src/app/pages/apps/gallery/gallery.component'
import { AppsMailComponent } from 'src/app/pages/apps/mail/mail.component'
import { WorkerComponent } from './worker/worker.component'
import { TicketsComponent } from './tickets/tickets.component'
import { WorkerCreateComponent } from './worker-create/worker-create.component'
import { WorkerEditComponent } from './worker-edit/worker-edit.component'
import { TicketShowComponent } from './ticket-show/ticket-show.component'

const COMPONENTS = [
  AppsMessagingComponent,
  AppsCalendarComponent,
  AppsProfileComponent,
  AppsGalleryComponent,
  AppsMailComponent,
  WorkerComponent,
  WorkerCreateComponent,
  WorkerEditComponent,
  TicketsComponent,
  TicketShowComponent,
]

@NgModule({
  imports: [
    SharedModule,
    AppsRouterModule,
    FormsModule,
    PerfectScrollbarModule,
    WidgetsComponentsModule,
    ReactiveFormsModule,
  ],
  declarations: [...COMPONENTS],
})
export class AppsModule {}
