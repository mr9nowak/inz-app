import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service'
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import { NzNotificationService } from 'ng-zorro-antd'
import {Router} from '@angular/router'

@Component({
  selector: 'app-worker-create',
  templateUrl: './worker-create.component.html',
  styleUrls: ['./worker-create.component.scss']
})
export class WorkerCreateComponent implements OnInit {
  createForm: FormGroup

  constructor(
    private api: ApiService,
    private fb: FormBuilder,
    public router: Router,
    private notification: NzNotificationService
  ) { }

  ngOnInit() {
    this.createForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      first_name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
    })
  }

  submitForm(): void {
    if (this.createForm.valid) {
      this.api.createWorker(this.createForm.value).subscribe(
        data => {
          this.notification.success(
            'Utworzono',
            ''
          )
          this.router.navigate(['admin/workers'])
        },
        error => {
          this.notification.error(error.code, error.message)
        }
        )
    }

    for (const i in this.createForm.controls) {
      this.createForm.controls[i].markAsDirty();
      this.createForm.controls[i].updateValueAndValidity();
    }
  }

  getErrorMessage(field: string) {
    switch (field) {
      case 'email': return this.createForm.controls.email.hasError('required') ? 'Pole jest wymagane' :
        this.createForm.controls.email.hasError('email') ? 'Podaj prawidłowy adres e-mail' : '';
      case 'password': return this.createForm.controls.password.hasError('required') ? 'Pole jest wymagane' : '';
      case 'first_name': return this.createForm.controls.first_name.hasError('required') ? 'Pole jest wymagane' : '';
      case 'last_name': return this.createForm.controls.last_name.hasError('required') ? 'Pole jest wymagane' : '';
    }
  }
}
