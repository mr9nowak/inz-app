import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { DashboardRouterModule } from './dashboard-routing.module'
import { WidgetsComponentsModule } from 'src/app/components/widgets/widgets-components.module'
import { NestableModule } from 'ngx-nestable'
import { FormsModule } from '@angular/forms'
import { ChartistModule } from 'ng-chartist'

// dashboard
import { DashboardAnalyticsComponent } from 'src/app/pages/dashboard/analytics/analytics.component'
import { DashboardEcommerceComponent } from 'src/app/pages/dashboard/ecommerce/ecommerce.component'
import { DashboardHelpdeskComponent } from 'src/app/pages/dashboard/helpdesk/helpdesk.component'
import { DashboardStatisticsComponent } from 'src/app/pages/dashboard/statistics/statistics.component'
import { DashboardJiraComponent } from 'src/app/pages/dashboard/jira/jira.component'
import { DashboardCryptoComponent } from 'src/app/pages/dashboard/crypto/crypto.component'

const COMPONENTS = [
  DashboardAnalyticsComponent,
  DashboardEcommerceComponent,
  DashboardHelpdeskComponent,
  DashboardStatisticsComponent,
  DashboardJiraComponent,
  DashboardCryptoComponent,
]

@NgModule({
  imports: [
    SharedModule,
    DashboardRouterModule,
    WidgetsComponentsModule,
    NestableModule,
    FormsModule,
    ChartistModule,
  ],
  declarations: [...COMPONENTS],
})
export class DashboardModule {}
