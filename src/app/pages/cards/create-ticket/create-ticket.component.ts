import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {ApiService} from '../../../services/api.service'
import {Router} from '@angular/router'
import {NzNotificationService} from 'ng-zorro-antd'

@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.scss']
})
export class CreateTicketComponent implements OnInit {
  answers = {
    1: [
      {
        key: 'content',
        value: 'Treść',
      },
      {
        key: 'action',
        value: 'Działanie',
      },
      {
        key: 'expansion',
        value: 'Rozbudowa',
      },
    ],
    2: [
      {
        key: 'yes',
        value: 'Tak'
      },
      {
        key: 'no',
        value: 'Nie'
      },
    ],
    3: [
      {
        key: 'yes',
        value: 'Tak'
      },
      {
        key: 'no',
        value: 'Nie'
      },
    ],
    4: [
      {
        key: 'chrome',
        value: 'Google Chrome',
      },
      {
        key: 'firefox',
        value: 'Mozilla Firefox',
      },
      {
        key: 'opera',
        value: 'Opera',
      },
      {
        key: 'ie',
        value: 'Internet Explorer',
      },
      {
        key: 'edge',
        value: 'Edge',
      },
      {
        key: 'safari',
        value: 'Safari',
      },
      {
        key: 'other',
        value: 'inna',
      },
    ],
    5: [
      {
        key: 'pc',
        value: 'Komputer/Laptop',
      },
      {
        key: 'tablet',
        value: 'Tablet',
      },
      {
        key: 'phone',
        value: 'Telefon',
      },
    ],
    6: [
      {
        key: 'absent',
        value: 'nie występuje',
      },
      {
        key: 'disposable',
        value: 'jednorazowy',
      },
      {
        key: 'recurrent',
        value: 'powtarzalny',
      },
    ],
  }
  createForm: FormGroup;

  constructor(
    private api: ApiService,
    private fb: FormBuilder,
    private notification: NzNotificationService
  ) { }

  submitForm(): void {
    if (this.createForm.valid) {
      this.api.createTicket(this.createForm.value).subscribe((data) => {
        this.notification.success(
          'Utworzono',
          ''
        )
      },
        error => {
          this.notification.error(error.code, error.message)
        })
    }

    for (const i in this.createForm.controls) {
      this.createForm.controls[i].markAsDirty();
      this.createForm.controls[i].updateValueAndValidity();
    }
  }

  displayError(field: string) {
    return !this.createForm.get(field).valid && this.createForm.get(field).touched;
  }

  getErrorMessage(field: string) {
    switch (field) {
      case 'answer1': return this.createForm.controls.answer1.hasError('required') ? 'Pole jest wymagane' : '';
      case 'answer2': return this.createForm.controls.answer2.hasError('required') ? 'Pole jest wymagane' : '';
      case 'answer3': return this.createForm.controls.answer3.hasError('required') ? 'Pole jest wymagane' : '';
      case 'answer4': return this.createForm.controls.answer4.hasError('required') ? 'Pole jest wymagane' : '';
      case 'answer5': return this.createForm.controls.answer5.hasError('required') ? 'Pole jest wymagane' : '';
      case 'answer6': return this.createForm.controls.answer6.hasError('required') ? 'Pole jest wymagane' : '';
      case 'content': return this.createForm.controls.content.hasError('required') ? 'Pole jest wymagane' : '';
      case 'email': return this.createForm.controls.email.hasError('required') ? 'Pole jest wymagane' :
        this.createForm.controls.email.hasError('email') ? 'Podaj prawidłowy adres e-mail' : '';
    }
  }

  ngOnInit() {
    this.createForm = this.fb.group({
      answer1: [null, [Validators.required]],
      answer2: [null, [Validators.required]],
      answer3: [null, [Validators.required]],
      answer4: [null, [Validators.required]],
      answer5: [null, [Validators.required]],
      answer6: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      content: [null, [Validators.required]],
    })
  }
}
