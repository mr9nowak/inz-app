import { Deserializable } from './deserializable.model'
import { Worker } from './worker'

export class Ticket implements Deserializable {
  uuid: string
  email: string
  content: string
  answer1: string
  answer2: string
  answer3: string
  answer4: string
  answer5: string
  answer6: string
  estimate_time: number
  spend_time: number
  state: string
  worker: Worker|null
  created_at: Date

  deserialize(input: any): this {
    Object.assign(this, input)

    if (input.worker) {
      this.worker = new Worker().deserialize(input.worker)
    }

    return this
  }

  public getAnswer1() {
    switch (this.answer1) {
      case 'content': return 'Treść'
      case 'action': return 'Działanie'
      case 'expansion': return 'Rozbudowa'
    }
  }

  public getAnswer2() {
    switch (this.answer2) {
      case 'yes': return 'Tak'
      case 'no': return 'Nie'
    }
  }

  public getAnswer3() {
    switch (this.answer3) {
      case 'yes': return 'Tak'
      case 'no': return 'Nie'
    }
  }

  public getAnswer4() {
    switch (this.answer4) {
      case 'chrome': return 'Google Chrome'
      case 'firefox': return 'Mozilla Firefox'
      case 'opera': return 'Opera'
      case 'ie': return 'Internet Explorer'
      case 'edge': return 'Edge'
      case 'safari': return 'Safari'
      case 'other': return 'inna'
    }
  }

  public getAnswer5() {
    switch (this.answer5) {
      case 'pc': return 'Komputer/Laptop'
      case 'tablet': return 'Tablet'
      case 'phone': return 'Telefon'
    }
  }

  public getAnswer6() {
    switch (this.answer6) {
      case 'absent': return 'nie występuje'
      case 'disposable': return 'jednorazowy'
      case 'recurrent': return 'powtarzalny'
    }
  }

  public getState() {
    switch (this.state) {
      case 'closed': return 'Zamknięte'
      case 'opened': return 'Otwarte'
    }
  }
}
