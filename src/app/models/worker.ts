import {Deserializable} from './deserializable.model'

export class Worker implements Deserializable {
  uuid: string
  first_name: string
  last_name: string
  email: string

  deserialize(input: any): this {
    Object.assign(this, input)

    return this
  }

  public getFullName(): string {
    return `${this.first_name} ${this.last_name}`
  }
}
